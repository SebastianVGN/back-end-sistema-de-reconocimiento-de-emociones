'use strict';

const databaseManager = require('./databaseManager');
const uuidv1 = require('uuid/v1');

exports.api = async (event) => {
	console.log(event);

	switch (event.httpMethod) {		
		case 'GET':
			return getPrueba(event);
		case 'POST':
			return savePrueba(event);
		default:
			return sendResponse(404, `Unsupported method "${event.httpMethod}"`);
	}
};

//GET: Leer todos los objetos u objeto especifico en DynamoDB
function getPrueba(event) {
	//Leer todos los objetos
	if (!event.pathParameters){
		console.log("Lectura Scan");	
		return databaseManager.getAllItems().then(response => {
			console.log("response");
			console.log(response);
			return sendResponse(200, {
				"message":"Pruebas escaneados correctamente",
				"Pruebas": response
			});		
		});
	}

	//Leer todos los objetos en una fecha
	const fecha = event.pathParameters.id;
	console.log(fecha);
	return databaseManager.getItemsByDate(fecha).then(response => {
		console.log("response");
		console.log(response);
		return sendResponse(200, {
			"message":"Pruebas escaneadas correctamente por fecha",
			"Pruebas": response
		});
	});
}

//POST: Guardar nuevo objeto en DynamoDB
function savePrueba(event) {
	const requestBody = JSON.parse(event.body);
 	const nombreReferencia = requestBody.nombreReferencia;

	if (typeof nombreReferencia !== 'string') {
		console.error('Error en los campos enviados');
		return sendResponse(400, {"message":"Debe incluirse un nombre de referencia en la prueba"});
	}

	const item = requestBody;
	item.id = uuidv1();

	var d = new Date();
	d.setHours(d.getHours()- 5);
	let formatted_date = d.getFullYear() + "-" + appendLeadingZeroes(d.getMonth() + 1) + "-" + appendLeadingZeroes(d.getDate())
	item.fecha = formatted_date;

	return databaseManager.saveItem(item).then(response => {
		console.log(response);
		return sendResponse(200, {
			"message":"Prueba registrada correctamente",
			"id":item.id
		});
	});
}

function sendResponse(statusCode, message) {
	const response = {
		statusCode: statusCode,
		headers: {
			"Access-Control-Allow-Origin":"*",
			"Access-Control-Request-Method":"*",
			"Access-Control-Request-Headers":"*"
		},
		body: JSON.stringify(message)
	};
	return response
}

function appendLeadingZeroes(n){
	if(n <= 9){
		return "0" + n;
	}
	return n
}
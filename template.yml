AWSTemplateFormatVersion: '2010-09-09'
Transform: 'AWS::Serverless-2016-10-31'

Globals:
  Function:
    Runtime: nodejs12.x
    Timeout: 10

Parameters:
  ProjectName:
    Type: String
    Default: 'FaceRekognition'
    Description: Name of the project this stack belongs to

Resources:
  MyApi:
    Type: AWS::Serverless::Api
    Properties:
      StageName: !Ref ProjectName
      Cors:
        AllowMethods: "'*'"
        AllowHeaders: "'*'"
        AllowOrigin: "'*'"
      EndpointConfiguration:
        Type: REGIONAL

  #Función Lambda y DynamoDB del servicio de Usuarios
  LambdaFunctionUsuarios:
    Type: 'AWS::Serverless::Function'
    Properties:
      Handler: handler.api
      CodeUri: ./ServiceUsuarios
      Policies:
        - DynamoDBCrudPolicy:
            TableName: !Ref DynamoDBTableUsuarios
      Environment:
        Variables:
          TABLE_NAME: !Ref DynamoDBTableUsuarios
      Events:
        CodeGetAPI:
          Type: Api
          Properties:
            RestApiId: !Ref MyApi
            Path: /Usuarios/{id}
            Method: GET
        AnyMethodAPI:
          Type: Api
          Properties:
            RestApiId: !Ref MyApi
            Path: /Usuarios/
            Method: ANY

  DynamoDBTableUsuarios:
    Type: 'AWS::DynamoDB::Table'
    Properties:
      AttributeDefinitions:
        - AttributeName: id
          AttributeType: S
        - AttributeName: usuario
          AttributeType: S

      KeySchema:
        - AttributeName: id
          KeyType: HASH

      GlobalSecondaryIndexes:
        - IndexName: indexUsuario
          KeySchema:
            - AttributeName: usuario
              KeyType: HASH
          Projection:
            ProjectionType: "ALL"
          ProvisionedThroughput:
            ReadCapacityUnits: 1
            WriteCapacityUnits: 1
      ProvisionedThroughput:
        ReadCapacityUnits: 1
        WriteCapacityUnits: 1

  #Función Lambda y DynamoDB del servicio de Pruebas
  LambdaFunctionPruebas:
    Type: 'AWS::Serverless::Function'
    Properties:
      Handler: handler.api
      CodeUri: ./ServicePruebas
      Policies:
        - DynamoDBCrudPolicy:
            TableName: !Ref DynamoDBTablePruebas
      Environment:
        Variables:
          TABLE_NAME: !Ref DynamoDBTablePruebas
      Events:
        CodeGetAPI:
          Type: Api
          Properties:
            RestApiId: !Ref MyApi
            Path: /Pruebas/{id}
            Method: GET
        AnyMethodAPI:
          Type: Api
          Properties:
            RestApiId: !Ref MyApi
            Path: /Pruebas/
            Method: ANY

  DynamoDBTablePruebas:
    Type: 'AWS::DynamoDB::Table'
    Properties:
      AttributeDefinitions:
        - AttributeName: id
          AttributeType: S
        - AttributeName: fecha
          AttributeType: S
      KeySchema:
        - AttributeName: id
          KeyType: HASH
      
      GlobalSecondaryIndexes:
        - IndexName: indexFecha
          KeySchema:
            - AttributeName: fecha
              KeyType: HASH
          Projection:
            ProjectionType: "ALL"
          ProvisionedThroughput:
            ReadCapacityUnits: 1
            WriteCapacityUnits: 1
      ProvisionedThroughput:
        ReadCapacityUnits: 1
        WriteCapacityUnits: 1

  #Función Lambda y DynamoDB del servicio de Muestras
  LambdaFunctionMuestras:
    Type: 'AWS::Serverless::Function'
    Properties:
      Handler: handler.api
      CodeUri: ./ServiceMuestras
      MemorySize: 300
      Policies:
        - DynamoDBCrudPolicy:
            TableName: !Ref DynamoDBTableMuestras
        - S3CrudPolicy:
            BucketName: !Ref S3BucketMuestras
        - AmazonRekognitionFullAccess
      Environment:
        Variables:
          TABLE_NAME: !Ref DynamoDBTableMuestras
          BUCKET_NAME: !Ref S3BucketMuestras
      Events:
        CodeGetAPI:
          Type: Api
          Properties:
            RestApiId: !Ref MyApi
            Path: /Muestras/{op}/{id}
            Method: GET
        AnyMethodAPI:
          Type: Api
          Properties:
            RestApiId: !Ref MyApi
            Path: /Muestras/
            Method: ANY

  DynamoDBTableMuestras:
    Type: 'AWS::DynamoDB::Table'
    Properties:
      AttributeDefinitions:
        - AttributeName: id
          AttributeType: S
        - AttributeName: id_prueba
          AttributeType: S

      KeySchema:
        - AttributeName: id
          KeyType: HASH

      GlobalSecondaryIndexes:
        - IndexName: IndexIdPrueba
          KeySchema:
            - AttributeName: id_prueba
              KeyType: HASH
          Projection:
            ProjectionType: "ALL"
          ProvisionedThroughput:
            ReadCapacityUnits: 1
            WriteCapacityUnits: 1
      ProvisionedThroughput:
        ReadCapacityUnits: 1
        WriteCapacityUnits: 1

  S3BucketMuestras:
    Type: 'AWS::S3::Bucket'
  
  #Función Lambda y DynamoDB del servicio de Sentimientos
  LambdaFunctionSentimientos:
    Type: 'AWS::Serverless::Function'
    Properties:
      Handler: handler.api
      CodeUri: ./ServiceSentimientos
      Policies:
        - DynamoDBCrudPolicy:
            TableName: !Ref DynamoDBTableSentimientos
      Environment:
        Variables:
          TABLE_NAME: !Ref DynamoDBTableSentimientos
      Events:
        CodeGetAPI:
          Type: Api
          Properties:
            RestApiId: !Ref MyApi
            Path: /Sentimientos/{id}
            Method: GET
        AnyMethodAPI:
          Type: Api
          Properties:
            RestApiId: !Ref MyApi
            Path: /Sentimientos/
            Method: ANY

  DynamoDBTableSentimientos:
    Type: 'AWS::DynamoDB::Table'
    Properties:
      AttributeDefinitions:
        - AttributeName: id
          AttributeType: S
      KeySchema:
        - AttributeName: id
          KeyType: HASH

      ProvisionedThroughput:
        ReadCapacityUnits: 1
        WriteCapacityUnits: 1

Outputs:
  # ServerlessRestApi is an implicit API created out of Events key under Serverless::Function
  # Find out more about other implicit resources you can reference within SAM
  # https://github.com/awslabs/serverless-application-model/blob/master/docs/internals/generated_resources.rst#api
  MyApi:
    Description: "API Gateway endpoint URL for Prod stage for My Lambda Function"
    Value: !Sub "https://${MyApi}.execute-api.${AWS::Region}.amazonaws.com/${ProjectName}/"
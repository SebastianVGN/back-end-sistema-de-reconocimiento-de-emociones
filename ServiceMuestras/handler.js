'use strict';

const databaseManager = require('./databaseManager');
const bucketManager = require('./bucketManager');
const rekognition = require('./rekognition');
const uuidv1 = require('uuid/v1');

exports.api = async (event) => {
	console.log(event);

	switch (event.httpMethod) {		
		case 'GET':
			if(!event.pathParameters){
				return getEmotions(event);			
			}else if (event.pathParameters.op=="Subtitulos"){
				return getSubtitulos(event);
			}else if (event.pathParameters.op=="Prueba"){
				return getEmotionsIdPrueba(event);
			}else{
				return sendResponse(404, "Ruta no reconocida");
			}
		case 'POST':
			return saveEmotion(event);
		default:
			return sendResponse(404, `Unsupported method "${event.httpMethod}"`);
	}
};

//GET: Leer todos los objetos
function getEmotions(event) {
	//Leer todos los objetos
	if (!event.pathParameters){
		console.log("Lectura Scan");	
		return databaseManager.getAllItems().then(response => {
			console.log("response");
			console.log(response);
			return sendResponse(200, {
				"message":"Muestras escaneados correctamente",
				"Muestras": response
			});		
		});
	}	
}

//GET: Leer todos los objetos de una prueba especifica
function getEmotionsIdPrueba(event) {
	const idprueba = event.pathParameters.id;
	console.log(idprueba);
	return databaseManager.getItemsByIdPrueba(idprueba).then(response => {
		console.log("response");
		console.log(response);
		response.sort(function(a, b){return a.segundo - b.segundo});

		response.forEach(agregarAtributosAdicionales);

		return sendResponse(200, {
			"message":"Muestras escaneadas correctamente por id_prueba",
			"Muestras": response
		});
	});
}

//POST: Guardar nuevo objeto en DynamoDB
async function saveEmotion(event) {
	const requestBody = JSON.parse(event.body);
	const id_prueba = requestBody.id_prueba;
	const segundo = requestBody.segundo;
	const imagen = requestBody.imagen; 

	if (typeof id_prueba !== 'string' || typeof segundo !== 'number' || typeof imagen !== 'string') {
	 	console.error('Error en los campos enviados');
	 	return sendResponse(400, {"message":"No se llenaron todos los campos obligatorios o se llenaron con un formato incorrecto"});
	}

	const item = requestBody;
	const imageBase64 = Buffer.from(imagen.replace(/^data:image\/\w+;base64,/, ""),'base64')

	item.id = uuidv1();
	delete item['imagen'];
	console.log("A punto de entrar")
	try{
		var result = await rekognition.detectEmotions(imageBase64);
		item.emocion = result.emocion;
		item.confianza = result.confianza;
		return databaseManager.saveItem(item).then(response => {
			console.log(response);
			return sendResponse(200, {
				"message":"Muestra registrada correctamente",
				"id": item.id,
				"emocion": item.emocion,
				"confianza": item.confianza
			});
		});
	} catch (e){
		return sendResponse(400, e);
	}
}

//GET: Generar y enviar link de archivo de Subtitulos
async function getSubtitulos(event) {
	const idprueba = event.pathParameters.id;
	console.log(idprueba);

	var muestras = await databaseManager.getItemsByIdPrueba(idprueba).then(response => {
		console.log("función subtitulos");
		console.log("response");
		console.log(response);
		return response.sort(function(a, b){return a.segundo - b.segundo});	
	});

	var subtitulos = generarSubtitulos(muestras);
	console.log("Entrando a write file");
	var nombreArch;
	try{
		nombreArch = await bucketManager.createSubtitulos(subtitulos, idprueba);
	} catch (e){
		return sendResponse(400, e);
	}

	try{
		var result = await bucketManager.createURL(nombreArch);
		return sendResponse(200, {
			"message":"Subtitulos generados correctamente",
			"URL": result
		});
	} catch (e){
		return sendResponse(400, e);
	}
}


/////////////////////////////////////Funciones de apoyo////////////////////////////////////////
function sendResponse(statusCode, message) {
	const response = {
		statusCode: statusCode,
		headers: {
			"Access-Control-Allow-Origin":"*",
			"Access-Control-Request-Method":"*",
			"Access-Control-Request-Headers":"*"
		},
		body: JSON.stringify(message)
	};
	return response
}

function time_convert(secs)
 { 
  var hours = Math.floor(secs / 3600);  
  var minutes = Math.floor((secs % 3600)/60);
  var seconds = Math.floor((secs % 3600) % 60);
  return appendLeadingZeroes(hours) + ":" + appendLeadingZeroes(minutes) + ":" + appendLeadingZeroes(seconds);         
}

function appendLeadingZeroes(n){
	if(n <= 9){
		return "0" + n;
	}
	return n
}

function agregarAtributosAdicionales(item, index, arr) {
	item.tiempo = time_convert(item.segundo);
	item.emocion = dictEmotions[item.emocion];
	item.porcentaje = Math.round(item.confianza) + "%"
}

function generarSubtitulos(item) {
	var subtitulos = "";
	var arrayLength = item.length;
	for (var i = 0; i < arrayLength; i++) {
		var orden = i + 1;
		subtitulos = subtitulos + orden + "\n" + time_convert(item[i].segundo) + " --> " + time_convert(item[i].segundo + 3) + "\n" + dictEmotions[item[i].emocion] + " - " + Math.round(item[i].confianza) + "%";
		if (orden != arrayLength){
			subtitulos = subtitulos + "\n\n"
		}
	}
	return subtitulos;
}

var dictEmotions = {
	"HAPPY": "FELICIDAD",
	"SAD": "TRISTEZA",
	"ANGRY": "MOLESTIA",
	"CONFUSED": "CONFUSIÓN",
	"DISGUSTED": "DISGUSTO",
	"SURPRISED": "SORPRESA",
	"CALM": "CALMA",
	"UNKNOWN": "NEUTRAL",
	"FEAR": "TEMOR"
};
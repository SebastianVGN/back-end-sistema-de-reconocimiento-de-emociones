'use strict';

const AWS = require('aws-sdk');
const s3 = new AWS.S3({});

module.exports.createSubtitulos  = function (contenido, nombre) {
    // I'm using the following method inside an async function.
    var params = {
        Bucket: process.env.BUCKET_NAME,
        Key: "subtitulos-" + nombre + ".srt",
        ContentType:'binary/octet-stream',
        Body: Buffer.from(contenido, 'binary')
    };

    return new Promise((resolve, reject) => {
        s3.upload(params, function(err, data) {
            if (err) {
                console.log("err");
				console.log(err.message);
				reject(err);
				return;
            }
            console.log(`File uploaded successfully. ${data.Location}`);
            console.log("tudo ben");
            resolve(params.Key)
        });
	});
}

module.exports.createURL  = function (nombre) {
    // I'm using the following method inside an async function.
    var params = {
        Bucket: process.env.BUCKET_NAME,
        Key: nombre,
        Expires: 60 * 5
    };

    return new Promise((resolve, reject) => {
        s3.getSignedUrl('getObject', params, function(err, url) {
            if (err) {
                console.log("err");
				console.log(err.message);
				reject(err);
				return;
            }
            console.log(`URL created successfully. ${url}`);
            console.log("tudo ben");
            resolve(url)
        });
	});
}
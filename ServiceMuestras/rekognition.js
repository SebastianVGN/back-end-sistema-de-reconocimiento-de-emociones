const AWS = require('aws-sdk')
const bucket = 'bucket-prueba-rek' // the bucketname without s3://

const client = new AWS.Rekognition();


module.exports.detectEmotions = function (imagen) {
    const params = {
        Image:  {
            Bytes: imagen
            },       
        Attributes: ['ALL']
    }
    console.log("Dentro de la función")
    return new Promise((resolve, reject) => {
        client.detectFaces(params, function(err, response) {
            if (err) {
                console.log("err stack");
                console.log(err, err.stack); // an error occurred
                console.log("err message");
				console.log(err.message);
				reject(err);
				return;
            } else {
                response.FaceDetails.forEach(data => {
                    console.log(` Emotions[0].Type: ${data.Emotions[0].Type}`)
                    console.log(` Emotions[0].Confidence: ${data.Emotions[0].Confidence}`)

                    var result = {
                        "emocion" : data.Emotions[0].Type ,
                        "confianza" : data.Emotions[0].Confidence
                    }

                    resolve(result)
                }) // for response.faceDetails
            } 
        });
    });
}
'use strict';

const AWS = require('aws-sdk');
let dynamo = new AWS.DynamoDB.DocumentClient();

const TABLE_NAME = process.env.TABLE_NAME;

module.exports.initializateDynamoClient = newDynamo => {
	dynamo = newDynamo;
};

module.exports.saveItem = item => {
	const params = {
		TableName: TABLE_NAME,
		Item: item
	};

	return dynamo
		.put(params)
		.promise()
		.then(() => {
			return item.id;
		});
};

module.exports.getAllItems = id => {
	const params = {
		TableName: TABLE_NAME
	};

	return dynamo
		.scan(params)
		.promise()
		.then(result => {
			return result.Items;
		});
};

module.exports.getItem = id => {
	const params = {
		Key: {
			id: id
		},
		TableName: TABLE_NAME
	};

	return dynamo
		.get(params)
		.promise()
		.then(result => {
			return result.Item;
		});
};

module.exports.getItemsByIdPrueba = idprueba => {
	const params = {
		TableName: TABLE_NAME,
		IndexName : "IndexIdPrueba",
		KeyConditionExpression: "#id_prueba = :v_id_prueba", 
		ExpressionAttributeNames:{
			"#id_prueba": "id_prueba"
		},
		ExpressionAttributeValues: {
			":v_id_prueba": idprueba
		}
	};

	return dynamo
		.query(params)
		.promise()
		.then(result => {
			console.log(result);
			return result.Items;
		});
};
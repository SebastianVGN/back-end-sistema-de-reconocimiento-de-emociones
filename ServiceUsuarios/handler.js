'use strict';

const databaseManager = require('./databaseManager');
const uuidv1 = require('uuid/v1');

exports.api = async (event) => {
	console.log(event);

	switch (event.httpMethod) {		
		case 'GET':
			return getRequest(event);
		case 'POST':
			return saveRequest(event);
		case 'PUT':
			return updateRequest(event);
		case 'DELETE':
			return deleteRequest(event);
		default:
			return sendResponse(404, `Unsupported method "${event.httpMethod}"`);
	}
};

//GET: Leer todos los objetos u objeto especifico en DynamoDB
function getRequest(event) {
	//Leer todos los objetos
	if (!event.pathParameters){
		console.log("Lectura Scan");	
		return databaseManager.getAllItems().then(response => {
			console.log("response");
			console.log(response);
			return sendResponse(200, {
				"message":"Objetos escaneados correctamente",
				"item": response
			});		
		});
	}

	//Leer objeto especifico
	const id = event.pathParameters.id;
	console.log(id);
	return databaseManager.getItem(id).then(response => {
		console.log("response");
		console.log(response);
		return sendResponse(200, {
			"message":"Objeto escaneado correctamente",
			"item": response
		});
	});
}

//POST: Guardar nuevo objeto en DynamoDB
function saveRequest(event) {
	const requestBody = JSON.parse(event.body);
 	const nombre = requestBody.nombre;
	const apellido = requestBody.apellido;
	const usuario = requestBody.usuario;
 	const contraseña = requestBody.contraseña;

	if (typeof nombre !== 'string' || typeof apellido !== 'string' || typeof usuario !== 'string' || typeof contraseña !== 'string') {
		console.error('Error en los campos enviados');
		return sendResponse(400, {"message":"No se llenaron todos los campos obligatorios o se llenaron con un formato incorrecto"});
	}

	const item = requestBody;
	item.id = uuidv1();

	return databaseManager.saveItem(item).then(response => {
		console.log(response);
		return sendResponse(200, {
			"message":"Objeto creado correctamente",
			"id":item.id
		});
	});
}

//PUT: Actualizar objeto especifico en DynamoDB
function updateRequest(event) {
	const requestBody = JSON.parse(event.body);
	const id = requestBody.id;
	const nombre = requestBody.nombre;
	const apellido = requestBody.apellido;
	const usuario = requestBody.usuario;
 	const contraseña = requestBody.contraseña;

	if (typeof id !== 'string') {
		console.error('Es necesario enviar el id del objeto');
		return sendResponse(400, {"message":"Es necesario enviar el id del objeto"});
	}
	 
	if (typeof nombre !== 'string' || typeof apellido !== 'string' || typeof usuario !== 'string' || typeof contraseña !== 'string') {
		console.error('Error en los campos enviados');
		return sendResponse(400, {"message":"No se llenaron todos los campos obligatorios o se llenaron con un formato incorrecto"});
	}

	const item = requestBody;
	return databaseManager.updateItem(item).then(response => {
		console.log(response);
		return sendResponse(200, {
			"message":"Objeto actualizado correctamente",
			"item": response
		});
	});
}

//DELETE: Eliminar objeto especifico en DynamoDB
function deleteRequest(event) {
	const requestBody = JSON.parse(event.body);
	const id = requestBody.id;

	if (typeof id !== 'string') {
		console.error('Es necesario enviar el id del objeto');
		return sendResponse(400, {"message":"Es necesario enviar el id del objeto"});
	}

	return databaseManager.deleteItem(id).then(response => {
		return sendResponse(200, {
			"message":"Objeto eliminado correctamente",
			"item": response
		});
	});
}

function sendResponse(statusCode, message) {
	const response = {
		statusCode: statusCode,
		headers: {
			"Access-Control-Allow-Origin":"*",
			"Access-Control-Request-Method":"*",
			"Access-Control-Request-Headers":"*"
		},
		body: JSON.stringify(message)
	};
	return response
}

'use strict';

const AWS = require('aws-sdk');
let dynamo = new AWS.DynamoDB.DocumentClient();

const TABLE_NAME = process.env.TABLE_NAME;

module.exports.initializateDynamoClient = newDynamo => {
	dynamo = newDynamo;
};

module.exports.saveItem = item => {
	item.ultimaModificacion = Math.floor(Date.now() / 1000);
	const params = {
		TableName: TABLE_NAME,
		Item: item
	};

	return dynamo
		.put(params)
		.promise()
		.then(() => {
			return item.id;
		});
};

module.exports.getAllItems = id => {
	const params = {
		TableName: TABLE_NAME
	};

	return dynamo
		.scan(params)
		.promise()
		.then(result => {
			return result.Items;
		});
};

module.exports.getItem = id => {
	const params = {
		Key: {
			id: id
		},
		TableName: TABLE_NAME
	};

	return dynamo
		.get(params)
		.promise()
		.then(result => {
			return result.Item;
		});
};

module.exports.updateItem = (item) => {
	const params = {
		TableName: TABLE_NAME,
		Key: {
			"id":item.id
		},
		ConditionExpression: 'attribute_exists(id)',
		UpdateExpression: 'set nombre = :n, ultimaModificacion = :t',
		ExpressionAttributeValues: {
			":n": item.nombre,
			":t": Math.floor(Date.now() / 1000)
		},
		ReturnValues: 'ALL_NEW'
	};

	return dynamo
		.update(params)
		.promise()
		.then(response => {
			return response.Attributes;
		});
};

module.exports.deleteItem = id => {
	const params = {
		Key: {
			id: id
		},
		TableName: TABLE_NAME
	};

	return dynamo.delete(params).promise();
};
